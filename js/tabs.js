document.querySelectorAll('.catalog__tab').forEach((item) =>
    item.addEventListener('click',function (e){
        e.preventDefault()
        const id = e.target.getAttribute('href').replace('#','')

        document.querySelectorAll('.catalog__tab').forEach((child)=>
            child.classList.remove("catalog__tab_active")
        )
        document.querySelectorAll('.catalog__content-wrapper').forEach((child)=>
            child.classList.remove("catalog__content-wrapper_active")
        )

        item.classList.add('catalog__tab_active');
        document.getElementById(id).classList.add('catalog__content-wrapper_active')
    })
)

document.querySelector('.catalog__tab').click()
