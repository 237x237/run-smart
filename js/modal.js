const UI_ELEMENTS ={
    CONSULTATION: document.querySelectorAll('[data-modal="consultation"]'),
    OVERLAY: document.querySelector('.overlay'),
    MODAL_CONSULTATION: document.querySelector('#consultation'),
    MODAL_CLOSE: document.querySelectorAll('.modal__close'),
    MODAL_ORDER: document.querySelector('#order'),
    BUY_BUTTONS: document.querySelectorAll('.catalog-item__button'),
    BLOCKS_PULSE: document.querySelectorAll('.catalog-item'),
}

UI_ELEMENTS.CONSULTATION.forEach((buttonItem) => buttonItem.addEventListener('click',openModalConsultation ));


function openModalConsultation(){
    UI_ELEMENTS.OVERLAY.style.display = "block";
    UI_ELEMENTS.MODAL_CONSULTATION.style.display = "block";
}

UI_ELEMENTS.BUY_BUTTONS.forEach((buttonItem) => buttonItem.addEventListener('click',openModalOrder )
);
//TODO: доделать часть с отображением часов

function openModalOrder(){
    UI_ELEMENTS.OVERLAY.style.display = "block";
    UI_ELEMENTS.MODAL_ORDER.style.display = "block";
}
UI_ELEMENTS.MODAL_CLOSE.forEach((closeItem) => closeItem.addEventListener('click', closeModal));

function closeModal (){
    UI_ELEMENTS.OVERLAY.style.display = "none";
    UI_ELEMENTS.MODAL_CONSULTATION.style.display = "none";
    UI_ELEMENTS.MODAL_CONSULTATION.style.display = "none";
    UI_ELEMENTS.MODAL_ORDER.style.display = "none";
}