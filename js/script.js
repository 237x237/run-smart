$(document).ready(function(){
    $('.slider__list').slick(
        {
            infinite: true,
            speed: 1000,
            adaptiveHeight: true,
            prevArrow: '<button type="button" class="slick-prev"><img src="../icon/left.png" alt=""></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="../icon/right.png" alt=""></button>'
        }
    );


    function validateForms(form){
        $(form).validate(
            {
                rules: {
                    name: "required",
                    phone: "required",
                    email: {
                        required: true,
                        email: true,
                        minlength: 2,
                    },
                },
                messages: {
                    name: "Пожалуйста, введите свое имя",
                    phone: "Пожалуйста, введите свой номер",
                    email: {
                        required: "Пожалуйста, введите свою почту",
                        email: "Неправильно введен адрес почты"
                    },
                    minlength: jQuery.validator.format("Введите {0} символа!")
                },

            }
        );
    }
    validateForms('#consultation_modal')
    validateForms('.consultation form')
    validateForms('#order form')

    $('form').submit(function (e) {
        e.preventDefault()

        if(!$(this).valid()){
            return ;
        }

        $.ajax({
            type: "POST",
            url: "mailer/smart.php",
            data: $(this).serialize()
        }).done(function (){
            $(this).find("input").val("");
            $('#order,#consultation').fadeOut()
            $('.overlay, #thanks').fadeIn('slow')

            $('form').trigger('reset')
        })
        return false
    })

});

